#include "gtest/gtest.h"
#include "../src/funcs.cpp"

TEST(FuncsTest,Add){
 EXPECT_EQ(6,add(4,2));
};
TEST(FuncsTest,Add2){
 EXPECT_EQ(8,add2(4,2));
};
TEST(FuncsTest,Sub){
 EXPECT_EQ(2,sub(4,2));
};
TEST(FuncsTest,Mul){
 EXPECT_EQ(8,mul(4,2));
};
TEST(FuncsTest,Div){
 EXPECT_EQ(2,dv(4,2));
};

int main(int argc,char**argv)
{
 testing::InitGoogleTest(&argc, argv);
 return RUN_ALL_TESTS();
}
