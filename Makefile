SRC=src/main.cpp
COV=coverage
OUT_T=test_output
OUT_E=expected_output
APP=program
CXX=g++
BROWSER=google-chrome
TESTS=tests
TEST_APP=runTests
TEST_SRC=$(TESTS)/test_funcs.cpp
LDFLAGS_GTEST=-lgtest -lpthread

# Default
all: distclean compile test

# Compiling section
compile:
	$(CXX) $(CXXFLAGS) -o $(APP) $(SRC)

compile_coverage:
	$(CXX) $(CXXFLAGS) -o $(APP) --coverage $(SRC)

# Testing section
test: simple_tests regression_test unit_tests

simple_tests:
	./$(APP) 1 4 2
	./$(APP) 2 4 2
	./$(APP) 4 4 2

regression_test:
	./$(APP) 1 4 2 | tail -n 1 > $(OUT_T)
	echo "c=6" > $(OUT_E)
	diff $(OUT_T) $(OUT_E)
	./$(APP) 2 4 2 | tail -n 1 > $(OUT_T)
	echo "c=2" > $(OUT_E)
	diff $(OUT_T) $(OUT_E)


unit_tests:
	$(CXX) $(CXXFLAGS) -o test_funcs.o -c $(TEST_SRC)
	$(CXX) $(CXXFLAGS) test_funcs.o -o $(TEST_APP) $(LDFLAGS) $(LDFLAGS_GTEST)
	./$(TEST_APP)

# Coverage section
coverage: clean clean_coverage compile_coverage simple_tests
	lcov -t "result" -o $(APP).info -c -d .
	genhtml -o $(COV) $(APP).info

show_cov: coverage
	$(BROWSER) $(COV)/index.html

# Cleaning section
distclean: clean clean_tests clean_coverage

.PHONY: clean clean_tests clean_coverage
clean:
	rm -f $(APP)

clean_tests:
	rm -rf $(TESTS)/Makefile $(TESTS)/CMakeFiles $(TESTS)/CMakeCache.txt $(TESTS)/cmake_install.cmake $(TESTS)/CMakeFiles $(TESTS)/runTests $(OUT_E) $(OUT_T) $(TEST_APP) *.o

clean_coverage:
	rm -rf *.gcno *.gcda $(APP).info $(COV) *.gcov
