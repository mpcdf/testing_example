#!/bin/bash

# already there
[[ -d googletest ]] && exit 0

# otherwise download and build
git clone https://github.com/google/googletest.git
mkdir -p googletest/build
cd googletest/build
cmake .. -DCMAKE_INSTALL_PREFIX=../install
make && make install
