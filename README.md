# Example project for testing and CI in gitlab

[![pipeline status](https://gitlab.mpcdf.mpg.de/mpcdf/testing_example/badges/master/pipeline.svg)](https://gitlab.mpcdf.mpg.de/mpcdf/testing_example/commits/master)
[![coverage report](https://gitlab.mpcdf.mpg.de/mpcdf/testing_example/badges/master/coverage.svg)](https://gitlab.mpcdf.mpg.de/mpcdf/testing_example/commits/master)


This is a little C++ project including some little functions, unit tests for the functions, and also some end-to-end regression tests.

For the unit tests, the [googletest framework](https://github.com/google/googletest) is used.
