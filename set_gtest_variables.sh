export GTEST_HOME=$PWD/googletest/install
export CXXFLAGS="$CXXFLAGS -I$GTEST_HOME/include"
export LDFLAGS="$LDFLAGS -L$GTEST_HOME/lib"
